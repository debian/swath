Source: swath
Section: text
Priority: optional
Maintainer: Theppitak Karoonboonyanan <thep@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 libdatrie-dev,
 libdatrie1-bin,
 pkgconf
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/debian/swath.git
Vcs-Browser: https://salsa.debian.org/debian/swath
Homepage: https://linux.thai.net/projects/swath
Standards-Version: 4.7.0

Package: swath
Architecture: any
Depends: swath-data, ${misc:Depends}, ${shlibs:Depends}
Suggests: libthai-data
Description: Thai word segmentation program
 Swath is a general-purpose utility for analyzing word boundaries in Thai
 text and inserting predefined word delimiter codes. It can be used as a
 filter for Thai LaTeX files so the lines are wrapped properly when processed
 with babel-thai macros. Other formats that swath can also handle include
 HTML, RTF and plain text.
 .
 This package contains the swath program.

Package: swath-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Data for swath Thai word segmentation program
 Swath is a general-purpose utility for analyzing word boundaries in Thai
 text and inserting predefined word delimiter codes. It can be used as a
 filter for Thai LaTeX files so the lines are wrapped properly when processed
 with babel-thai macros. Other formats that swath can also handle include
 HTML, RTF and plain text.
 .
 This package contains data files needed by the swath program.
